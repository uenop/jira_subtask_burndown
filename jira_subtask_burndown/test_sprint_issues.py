from random import random

import pandas as pd

from jira_subtask_burndown import SprintIssues


class Dotdict(dict):
    """dot.notation access to dictionary attributes"""
    __getattr__ = dict.get
    __setattr__ = dict.__setitem__
    __delattr__ = dict.__delitem__


class DummyJiraClient(object):
    def __init__(self):
        pass

    def search_issues(self, query, maxResults):
        def gen_item():
            created = pd.to_datetime(
                '2018-08-01') + pd.Timedelta('{} days'.format(int(30 * random())))
            item_fields = Dotdict(
                summary="サマリ",
                assignee="Me",
                aggregatetimeestimate=60,
                aggregatetimespent=60 * random(),
                created=created,
                resolution=None if random() < 0.5 else created,
                issuetype="subtask",
            )
            item = Dotdict(dict(fields=item_fields))

            return item

        return list(map(lambda _: gen_item(), range(30)))


def test_init():
    sprint_name = 'ほげ'
    project_name = 'proj1'
    jira = DummyJiraClient()
    si = SprintIssues(jira=jira, sprint_name=sprint_name, project_name=project_name)


def test_fetch():
    sprint_name = 'ほげ'
    project_name = 'proj1'
    jira = DummyJiraClient()
    si = SprintIssues(jira=jira, sprint_name=sprint_name, project_name=project_name)
    issues = si.fetch()

    assert isinstance(issues, list)

def test_fetch_parse():
    sprint_name = 'ほげ'
    project_name = 'proj1'
    jira = DummyJiraClient()
    si = SprintIssues(jira=jira, sprint_name=sprint_name, project_name=project_name)
    issues = si.fetch_parse()

    assert isinstance(issues, pd.DataFrame)

    for col in issues.columns:
        assert col in ['aggestimate', 'aggspent', 'assignee', 'issuetype', 'summary', 'created', 'createddate', 'resolutiondate']

