import pandas as pd


class IssuesProcessor(object):
    def __init__(self, issues):
        self.issues = issues

    def calc_subtask_count_burndown(self):
        """ subtaskの個数でバーンダウンの表を返す """

        df_issues = self.issues

        s_created_size = df_issues.groupby('createddate').size()
        s_resolution_size = df_issues.groupby('resolutiondate').size()

        # Create new dataframe with 2 columns, which has date index
        df = pd.DataFrame(dict(created_size=s_created_size, resolved_size=s_resolution_size))
        all_days = pd.date_range(df.index.min(), df.index.max(), freq='D')
        df = df.reindex(all_days, fill_value=0)
        df.index.name = 'created_or_resolved'

        return df.fillna(0).cumsum()

    def calc_estimate_spent(self):
        """ worklogを集計したDataFrameを返す """
        df_issues = self.issues

        df = df_issues.groupby('createddate')['aggestimate', 'aggspent'].sum()
        all_days = pd.date_range(df.index.min(), df.index.max(), freq='D')
        df = df.reindex(all_days, fill_value=0)
        df.index.name = 'created'

        df_cumu = df.cumsum()
        df_cumu = df_cumu.assign(spent_ratio=df_cumu.aggspent / df_cumu.aggestimate)
        df_cumu = df_cumu.fillna(0)

        return df_cumu
