import os
import pandas as pd

from jira_subtask_burndown import IssuesProcessor

test_data = os.path.join(os.path.dirname(os.path.realpath(__file__)), 'issues.csv')
issues = pd.read_csv(test_data, index_col=0, parse_dates=['createddate', 'resolutiondate'])

def test_init():
    assert list(issues.columns) == ['aggestimate', 'aggspent', 'assignee', 'issuetype', 'summary', 'created', 'createddate', 'resolution', 'resolutiondate']

    p = IssuesProcessor(issues)

def test_calc_subtask_count_burndown():
    p = IssuesProcessor(issues)
    df = p.calc_subtask_count_burndown()
    assert isinstance(df, pd.DataFrame)
    assert len(df.columns) == 2

def test_calc_estimate_spent():
    p = IssuesProcessor(issues)
    df = p.calc_estimate_spent()

    assert isinstance(df, pd.DataFrame)
    assert len(df.columns) == 3
