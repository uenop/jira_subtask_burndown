import matplotlib as mpl
mpl.use('agg')
import matplotlib.pyplot as plt
mpl.style.use('ggplot')
mpl.rcParams['figure.figsize'] = (10, 8)
mpl.rcParams['font.family'] = ['IPAPGothic']


class Chart(object):
    def __init__(self, df, *, prefix, title, colormap):
        self.df = df
        self.prefix = prefix
        self.title = title
        self.colormap = colormap

    def render(self, df):
        df.plot(title=self.title, colormap=self.colormap)

        self.df.to_csv(self.prefix + '.csv')
        print('Wrote ' + self.prefix + '.csv')

        plt.savefig(self.prefix + '.png')
        print('Wrote ' + self.prefix + '.png')

    def render_subtask_burndown(self):
        self.render(self.df)

    def render_worklog(self):
        cols = ['aggspent', 'aggestimate']
        self.render(self.df[cols])
