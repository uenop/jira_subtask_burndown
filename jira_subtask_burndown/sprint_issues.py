from datetime import datetime

import pandas as pd


class SprintIssues(object):
    def __init__(self, *, jira, project_name, sprint_name):
        self.jira = jira
        self.project_name = project_name
        self.sprint_name = sprint_name

    def fetch(self):
        query = 'project={project} AND sprint="{sprint}" AND type="サブタスク"'.format(
            project=self.project_name,
            sprint=self.sprint_name)
        issues = self.jira.search_issues(query, maxResults=100)

        return issues

    def _parse(self, issues_list):
        df = pd.DataFrame(dict(
            aggestimate=i.fields.aggregatetimeestimate,
            aggspent=i.fields.aggregatetimespent,
            assignee=i.fields.assignee,
            created=i.fields.created,
            issuetype=i.fields.issuetype,
            resolution=i.fields.resolution,
            summary=i.fields.summary,
        ) for i in issues_list)
#        df.index = pd.to_datetime(df.pop('created'))
        df = df.assign(createddate=df.created.dt.date)
        df = df.assign(resolutiondate=pd.to_datetime(df.pop('resolution')).dt.date)

        return df

    def fetch_parse(self):
        return self._parse(self.fetch())
