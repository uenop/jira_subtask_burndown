import sys
from setuptools import setup, find_packages
from setuptools.command.test import test as TestCommand


class PyTest(TestCommand):
    user_options = [('pytest-args=', 'a', "Arguments to pass to py.test")]

    def initialize_options(self):
        TestCommand.initialize_options(self)
        self.pytest_args = []

    def run_tests(self):
        # import here, cause outside the eggs aren't loaded
        import pytest
        errno = pytest.main(self.pytest_args)
        sys.exit(errno)


setup(
    cmdclass={'test': PyTest},
    install_requires=[
        'pandas>=0.23.0',
        'jira>=2.0.0',
    ],
    name="jira_subtask_burndown",
    packages=find_packages(),
    tests_require=['pytest>=3.7'],
    version="0.1.0",
)
